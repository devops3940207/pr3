# Используем базовый образ с Ubuntu
FROM ubuntu:latest

# Устанавливаем необходимые пакеты
RUN apt-get update && apt-get install -y dpkg

# Копируем .deb пакет в образ
COPY ./build/bin/*.deb /tmp/

# Устанавливаем .deb пакет
RUN dpkg -i /tmp/*.deb || true

CMD ["bash"]
